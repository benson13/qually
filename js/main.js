( function($) {

'use strict';

  window.addEventListener('load', function () {
    var video = document.querySelector('#video');
    var preloader = document.querySelector('.preloader');

    function checkLoad() {
      if (video.readyState === 4) {
        $('.preloader').fadeOut();
        video.play();
      } else {
        setTimeout(checkLoad, 4000);
      }
    }
    setTimeout( function() {
      checkLoad();
    }, 4000)
  }, false);

  $(document).ready(function () {

    $('.popup-youtube').fancybox();

    // $('.carousel-image').slick({
    //   asNavFor: '.carousel-info',
    //   autoplay: true,
    //   autoplaySpeed: 3000,
    //   infinite: false,
    //   slidesToShow: 4,
    //   slidesToScroll: 1,
    //   dots: false,
    // });
    
     $('.carousel-info').slick({
      // asNavFor: '.carousel-image',
      autoplay: true,
      autoplaySpeed: 3000,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: true,
    });


    function typeEffect(element, speed) {
      var text = $(element).text();
      $(element).html('');

      var i = 0;
      var timer = setInterval(function () {
        if (i < text.length) {
          $(element).append(text.charAt(i));
          i++;
        } else {
          clearInterval(timer);
        }
      }, speed);
    }

    $(document).ready(function () {

      $('.carousel-image').each(function (index) {
        setInterval(function() {

        }, 3000)
      });
    });
  });


    // Init ScrollMagic
    var controller = new ScrollMagic.Controller();


    // build a scene
    // var sectionList = new TimelineMax();
    // sectionList
    //   .staggerFrom('.animate-text', 1, { x: '-50%', autoAlpha: 0, ease: Power0.easeNone }, "-1")
    //   .staggerTo('.animate-text', 1, { x: '+50%', autoAlpha: 0, ease: Power0.easeNone }, "-1")
    // var ourScene = new ScrollMagic.Scene({
    //   triggerElement: '.item-2 .pin-wrapper',
    //   triggerHook: 0.5,
    //   duration: '100%'

    // })
    //   .setTween(sectionList)
    //   .addTo(controller);




    // build a scene
    var lastTitles = new TimelineMax();
    lastTitles
      .staggerFrom('.animate-text', 1, { cycle: { x: ["50%", "-50%"] }, autoAlpha: 0, ease: Power0.easeNone }, "-1")
      
      var ourScene = new ScrollMagic.Scene({
      triggerElement: '.item-3 .pin-wrapper',
      triggerHook: 0.5,
      duration: '50%'

    })
      .setTween(lastTitles)
      .addTo(controller);

    // pin the intro
    var mainTitle = new TimelineMax();
    mainTitle
      .from('.section-title', 0.2, { y: '0%', autoAlpha: 0, ease: Power0.easeNone }, 0)
      .to('.section-title', 1, { y: '-175px', autoAlpha: 0, ease: Power0.easeNone }, 0.2);


    var pinIntroScene = new ScrollMagic.Scene({
      triggerElement: '.item-1 .pin-wrapper',
      triggerHook: 0,
      // duration: '100%'
    })
      .setPin('.item-1 .pin-wrapper', { pushFollowers: false })
      .setTween(mainTitle)
      .addTo(controller);

    // pin 2
    var pinIntroScene = new ScrollMagic.Scene({
      triggerElement: '.item-2 .pin-wrapper',
      triggerHook: 1

    })
      .setPin('.item-2 .pin-wrapper', { pushFollowers: false })
      .addTo(controller);
    // pin 3
    var pinIntroScene = new ScrollMagic.Scene({
      triggerElement: '.item-3 .pin-wrapper',
      triggerHook: 1,

    })
      .setPin('.item-3 .pin-wrapper', { pushFollowers: false })
      .addIndicators({ name: "1 (duration: 300)" })
      .addTo(controller);

    // pin 4
    var pinIntroScene = new ScrollMagic.Scene({
      triggerElement: '.item-4 .pin-wrapper',
      triggerHook: 1,

    })
      .setPin('.item-4 .pin-wrapper', { pushFollowers: false })
      .addIndicators({ name: "1 (duration: 300)" })
      .addTo(controller);

    // pin 5
    var pinIntroScene = new ScrollMagic.Scene({
      triggerElement: '.item-5 .pin-wrapper',
      triggerHook: 1,
      duration: 300

    })
      .setPin('.item-5 .pin-wrapper', { pushFollowers: true })
      .addIndicators({ name: "1 (duration: 300)" })
      .addTo(controller);





})(jQuery)



